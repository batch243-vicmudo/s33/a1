//console.log("Hello World");

/*
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response)=>response.json())
.then((json)=>console.log(json.map((elem) => 
		{return elem.title})));*/


fetch('https://jsonplaceholder.typicode.com/todos')
.then( response => {
	return response.json();
})

.then( data => {

	const result = data.map ((item) => {
		
		return item.title;
		
		});
	
	console.log(result);

});

//5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

async function fetchData() {
	let result = await fetch('https://jsonplaceholder.typicode.com/todos/1',{
		method: 'GET',
		headers: {
			'Content-type':'application/json',
		}
		
	})
	.then((response)=>response.json())
	.then((json)=>console.log(json))
	let message = await console.log(`The item "delectus aut autem" on the list has a status of false`)
};

fetchData();

//7. fetch request using the Post method
fetch('https://jsonplaceholder.typicode.com/todos',{
		
		method: 'POST',

	
		headers : {
			'Content-type':'application/json'
		},
		body: JSON.stringify({
			completed: false,
			id: 201,
			title: "Created To Do List Item",
			userId: 1
		})
})
.then((response)=> response.json())
.then((json)=> console.log(json));

//Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
// Update a to do list item by changing the data structure to contain
// the following properties:
// a. Title
// b. Description
// c. Status
// d. Date Completed
// e. User ID


fetch('https://jsonplaceholder.typicode.com/todos/1',{
		method: 'PUT',
		headers: {
			'Content-type':'application/json',
		},
		body: JSON.stringify({
			dateCompleted: "Pending",
			description: "To update the my to do list with a different data structure..",
			id: 1,
			status: "Pending",
			title: "Update To Do List Item",
			userId: 1
		})
	})
	.then((response)=> response.json())
	.then((json)=> console.log(json));

	//Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
	//11. Update a to do list item by changing the status to complete and add a date when the status was changed.

	fetch('https://jsonplaceholder.typicode.com/todos/1',{
			method: 'PATCH',
			headers: {
				'Content-type':'application/json',
			},
			body: JSON.stringify({
				completed: false,
				dateCompleted: "07/09/21",
				id: 1,
				status: "Complete",
				title: "delectus aut autem",
				userId: 1
				
			})
		})
		.then((response)=> response.json())
		.then((json)=> console.log(json));

//Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API

		fetch('https://jsonplaceholder.typicode.com/todos/1',{
			method: 'DELETE'
			
		})